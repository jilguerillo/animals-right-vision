import '../styles/globals.css';

export const metadata = {
  title: 'Animal Right Vision',
  description: 'Unificando la ayuda animal y ambiental globalmente',
};

export default function RootLayout({ children }) {
  return (
    <html lang="es">
      <body>{children}</body>
    </html>
  );
}
