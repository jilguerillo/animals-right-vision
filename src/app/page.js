"use client";
import React, { useState } from "react";
import Carousel from "@/components/Carousel";
import SanctuaryCard from "@/components/SanctuaryCard";
import FilterBar from "@/components/FilterBar";
import Navbar from "@/components/Navbar";
import Footer from "@/components/Footer";
import ModalCarousel from "@/components/ui/ModalCarousel";
import Modal from "@/components/ui/Modal";
import { categories, sanctuaries } from "@/constants/data";

export default function Home() {
  const [isCarouselModalOpen, setIsCarouselModalOpen] = useState(false);
  const [carouselSanctuaries, setCarouselSanctuaries] = useState([]); // Estado solo para el modal
  const [filteredSanctuaries, setFilteredSanctuaries] = useState(sanctuaries); // Mantén todos los cards sin filtrado
  const [selectedSanctuary, setSelectedSanctuary] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);

  // Función para abrir el modal-carrusel con santuarios de la categoría seleccionada
  const openCategoryCarousel = (categoryId) => {
    // Filtramos solo para el modal
    const filtered = sanctuaries.filter((sanctuary) =>
      sanctuary.categoryId.includes(categoryId)
    );
    setCarouselSanctuaries(filtered); // Los santuarios filtrados solo se aplican al modal
    setIsCarouselModalOpen(true); // Abrimos el modal-carrusel
  };

  const closeCategoryCarousel = () => {
    setIsCarouselModalOpen(false);
  };

  // Función para abrir el modal individual de los santuarios
  const openSanctuaryModal = (sanctuary) => {
    setSelectedSanctuary(sanctuary);
    setIsModalOpen(true);
  };

  const closeSanctuaryModal = () => {
    setSelectedSanctuary(null);
    setIsModalOpen(false);
  };

  // Función para manejar el filtro de búsqueda en los cards (sin afectar el modal)
  const handleFilterChange = (searchTerm) => {
    if (!searchTerm) {
      setFilteredSanctuaries(sanctuaries); // Si no hay búsqueda, mostramos todos los santuarios
    } else {
      const filtered = sanctuaries.filter((sanctuary) =>
        sanctuary.name.toLowerCase().includes(searchTerm.toLowerCase())
      );
      setFilteredSanctuaries(filtered); // El filtro solo afecta a los cards de la página
    }
  };

  return (
    <div className="min-h-screen bg-gray-50 text-gray-900 p-4 w-full overflow-x-hidden">
      <Navbar />

      {/* Carrusel de Categorías */}
      <div className="container mx-auto mt-8">
        <h2 className="text-2xl font-bold mb-4 text-green-600">Explora por Categoría</h2>
        <Carousel categories={categories} onSelectCategory={openCategoryCarousel} />
      </div>

      {/* Barra de Filtros */}
      <div className="container mx-auto mt-8">
        <h2 className="text-2xl font-bold mb-4 text-green-600">Encuentra un Santuario</h2>
        <FilterBar onFilterChange={handleFilterChange} />
      </div>

      {/* Cards de Santuarios */}
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4 p-8">
        {filteredSanctuaries.map((sanctuary) => (
          <div
            key={sanctuary.id}
            className="cursor-pointer bg-gray-50 text-gray-900 rounded-lg  p-4"
            onClick={() => openSanctuaryModal(sanctuary)}
          >
            <SanctuaryCard sanctuary={sanctuary} />
          </div>
        ))}
      </div>

      {/* Modal-carrusel de Santuarios */}
      <ModalCarousel
        isOpen={isCarouselModalOpen}
        onClose={closeCategoryCarousel}
        sanctuaries={carouselSanctuaries} 
      />

      {/* Modal individual del santuario */}
      {selectedSanctuary && (
        <Modal
          isOpen={isModalOpen}
          onClose={closeSanctuaryModal}
          sanctuary={selectedSanctuary}
        />
      )}

      <Footer />
    </div>
  );
}
