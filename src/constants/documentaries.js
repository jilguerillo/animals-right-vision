export const documentaries = [
    {
      id: 1,
      title: "Dominion",
      description: "Un documental innovador que expone la dura realidad de las prácticas agrícolas modernas.",
      videoUrl: "https://watch.unchainedtv.com/videos/dominion-1",
      imageUrl: "https://pics.filmaffinity.com/Dominion-821853423-large.jpg"
    },
    {
      id: 2,
      title: "Lucent",
      description: "Explora el lado oscuro de la industria porcina en Australia.",
      videoUrl: "https://watch.unchainedtv.com/videos/dominion-1",
      imageUrl: "https://i.ytimg.com/vi/KArL5YjaL5U/maxresdefault.jpg"
    },
    {
      id: 3,
      title: "Thousand Eyes",
      description: "Documental sobre la agricultura animal en Australia.",
      videoUrl: "https://watch.unchainedtv.com/videos/dominion-1",
      imageUrl: "/img_thousand_eyes.png" // Esta es la imagen local
    }
  ];
  