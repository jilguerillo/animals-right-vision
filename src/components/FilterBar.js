import React, { useState } from "react";
import { Search, XCircle } from "lucide-react";

export default function FilterBar({ onFilterChange }) {
  const [searchTerm, setSearchTerm] = useState("");

  const handleInputChange = (e) => {
    const value = e.target.value;
    setSearchTerm(value);
    onFilterChange(value); // Aquí pasamos el valor ingresado al callback onFilterChange
  };

  const clearSearch = () => {
    setSearchTerm("");
    onFilterChange(""); // Reiniciamos el filtro al estado original
  };

  return (
    <div className="relative flex items-center">
      {/* Icono de búsqueda */}
      <Search className="absolute left-3 text-gray-500 w-5 h-5" />

      {/* Campo de búsqueda */}
      <input
        type="text"
        placeholder="Buscar santuario..."
        value={searchTerm}
        onChange={handleInputChange}
        className="w-full pl-10 pr-10 py-2 border border-gray-300 rounded-md bg-white text-black focus:outline-none focus:ring-2 focus:ring-green-500 focus:border-transparent transition" // Cambiamos el fondo a blanco y el texto a negro
      />

      {/* Botón de limpiar búsqueda */}
      {searchTerm && (
        <button
          onClick={clearSearch}
          className="absolute right-3 text-gray-500 hover:text-gray-700 focus:outline-none"
        >
          <XCircle className="w-5 h-5" />
        </button>
      )}
    </div>
  );
}
