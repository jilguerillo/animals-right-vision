import Link from 'next/link';
import { Facebook, Twitter, Instagram, Youtube } from 'lucide-react';

export default function Footer() {
  return (
    <footer className="bg-[#384D48] text-[#E2E2E2] py-10 px-20"> {/* Fondo verde oscuro, texto gris claro */}
      <div className="grid grid-cols-1 md:grid-cols-4 gap-8">
        <div>
          <h3 className="font-semibold mb-2 text-[#ACAD94]">Animal Right Vision</h3>
          <p className="text-sm">Uniendo la ayuda animal y ambiental globalmente</p>
        </div>

        <div>
          <h3 className="font-semibold mb-2 text-[#ACAD94]">Enlaces</h3>
          <ul className="text-sm space-y-1">
            <li><Link href="/about" className="hover:text-[#ACAD94]">Acerca de</Link></li>
            <li><Link href="/contact" className="hover:text-[#ACAD94]">Contacto</Link></li>
            <li><Link href="/privacy" className="hover:text-[#ACAD94]">Privacidad</Link></li>
          </ul>
        </div>

        <div>
          <h3 className="font-semibold mb-2 text-[#ACAD94]">Síguenos</h3>
          <div className="flex space-x-2">
            <a href="https://facebook.com"><Facebook className="w-6 h-6 cursor-pointer hover:text-[#ACAD94]" /></a>
            <a href="https://twitter.com"><Twitter className="w-6 h-6 cursor-pointer hover:text-[#ACAD94]" /></a>
            <a href="https://instagram.com"><Instagram className="w-6 h-6 cursor-pointer hover:text-[#ACAD94]" /></a>
            <a href="https://youtube.com"><Youtube className="w-6 h-6 cursor-pointer hover:text-[#ACAD94]" /></a>
          </div>
        </div>

        <div>
          <h3 className="font-semibold mb-2 text-[#ACAD94]">Boletín</h3>
          <form className="flex">
            <input type="email" placeholder="Tu email" className="rounded-r-none bg-[#D8D4D5] text-black" />
            <button className="bg-[#ACAD94] text-white rounded-l-none py-1 px-4">Suscribir</button>
          </form>
        </div>
      </div>
      <div className="mt-8 text-center text-sm">© 2024 Animal Right Vision. Todos los derechos reservados.</div>
    </footer>
  );
}
