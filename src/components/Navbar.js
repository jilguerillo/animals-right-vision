import Link from 'next/link';
import { useState } from 'react';
import { MapPin, Menu, X } from 'lucide-react'; // Agregué el ícono del menú
import MapModal from '@/components/ui/MapModal';

export default function Navbar() {
  const [isMapModalOpen, setMapModalOpen] = useState(false);
  const [isMenuOpen, setIsMenuOpen] = useState(false); // Estado para el menú móvil

  const openMapModal = () => setMapModalOpen(true);
  const closeMapModal = () => setMapModalOpen(false);
  const toggleMenu = () => setIsMenuOpen(!isMenuOpen); // Toggle para el menú

  return (
    <>
      <nav className="bg-[#526964] w-full p-4 shadow-md font-mono">
        <div className="container mx-auto flex items-center justify-between">
          {/* Logo */}
          <div className="flex items-center">
            <Link href="/" className="text-2xl font-bold text-[#E2E2E2]">
              Animal Right Vision
            </Link>
          </div>

          {/* Links (versión desktop) */}
          <div className="hidden md:flex space-x-6">
            <Link href="/" className="hover:text-[#ACAD94] text-[#D8D4D5]">Formularios</Link>
            <Link href="/organizations" className="hover:text-[#ACAD94] text-[#D8D4D5]">Organizaciones</Link>
            <Link href="/about" className="hover:text-[#ACAD94] text-[#D8D4D5]">Acerca de</Link>
          </div>

          {/* Botón Ver Mapa */}
          <div className="hidden md:flex items-center space-x-4">
            <button
              className="bg-[#6E7271] text-white py-2 px-4 rounded-md hover:bg-[#ACAD94] flex items-center space-x-2 transition duration-200"
              onClick={openMapModal}
            >
              <MapPin className="w-5 h-5" />
              <span>Ver Mapa</span>
            </button>
          </div>

          {/* Menú hamburguesa (versión mobile) */}
          <div className="md:hidden">
            <button onClick={toggleMenu} className="text-[#E2E2E2]">
              {isMenuOpen ? <X className="w-6 h-6" /> : <Menu className="w-6 h-6" />}
            </button>
          </div>
        </div>

        {/* Menú desplegable (versión mobile) */}
        {isMenuOpen && (
          <div className="md:hidden mt-4 space-y-2">
            <Link href="/" className="block hover:text-[#ACAD94] text-[#D8D4D5]">Formularios</Link>
            <Link href="/organizations" className="block hover:text-[#ACAD94] text-[#D8D4D5]">Organizaciones</Link>
            <Link href="/about" className="block hover:text-[#ACAD94] text-[#D8D4D5]">Acerca de</Link>
            <button
              className="bg-[#6E7271] text-white py-2 px-4 rounded-md hover:bg-[#ACAD94] flex items-center space-x-2 transition duration-200 w-full"
              onClick={openMapModal}
            >
              <MapPin className="w-5 h-5" />
              <span>Ver Mapa</span>
            </button>
          </div>
        )}
      </nav>

      {/* Modal del Mapa */}
      <MapModal isOpen={isMapModalOpen} onClose={closeMapModal} />
    </>
  );
}
