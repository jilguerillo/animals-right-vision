export default function SanctuaryCard({ sanctuary, onOpenModal }) {
  return (
    <div className="bg-[#F5F5F5] text-[#384D48] font-mono  rounded-lg shadow-md p-4 hover:shadow-lg transition-transform transform hover:scale-105 w-full h-full overflow-hidden">
      <img
        src={sanctuary.imageUrl}
        alt={sanctuary.name}
        className="w-full h-48 object-contain rounded-t-lg cursor-pointer"
        onClick={onOpenModal}
      />
      <div className="mt-4">
        <h3 className="text-xl font-bold cursor-pointer hover:underline" onClick={onOpenModal}>
          {sanctuary.name}
        </h3>
        <p className="text-sm">{sanctuary.info}</p>
        <button
          onClick={onOpenModal}
          className="mt-4 bg-[#ACAD94] text-white py-1 px-4 rounded hover:bg-[#6E7271] transition duration-200"
        >
          Ver más
        </button>
      </div>
    </div>
  );
}
