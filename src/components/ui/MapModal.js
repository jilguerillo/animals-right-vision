import React from "react";
import { X } from "lucide-react";
import CountryMap from "@/components/map/CountryMap";

export default function ModalMap({ isOpen, onClose, countries }) {
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-70">
      <div className="bg-white text-black rounded-lg shadow-lg max-w-4xl w-full relative p-4">
        {/* Botón para cerrar el modal */}
        <button
          onClick={onClose}
          className="absolute top-4 right-4 text-gray-500 hover:text-gray-700 focus:outline-none"
        >
          <X className="w-6 h-6" />
        </button>

        {/* Título y Mapa */}
        <h2 className="text-xl font-bold mb-4 text-center">Explora Santuarios por País</h2>
        <div className="w-full h-[500px]">
          <CountryMap countries={countries} onSelectCountry={() => {}} />
        </div>
      </div>
    </div>
  );
}
