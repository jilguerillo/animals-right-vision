import React from "react";
import { X } from "lucide-react";

export default function Modal({ isOpen, onClose, sanctuary }) {
  if (!isOpen) return null;

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-70 font-mono w-full h-full overflow-y-hidden">
      <div className="bg-white text-black rounded-lg shadow-lg w-full max-w-lg sm:max-w-2xl sm:mx-4 mx-2 relative p-6">
        {/* Botón para cerrar el modal */}
        <button
          onClick={onClose}
          className="absolute top-4 right-4 text-gray-500 hover:text-gray-700 focus:outline-none"
        >
          <X className="w-6 h-6" />
        </button>

        {/* Contenido del modal */}
        <div className="flex flex-col items-center">
          <img
            src={sanctuary.imageUrl}
            alt={sanctuary.name}
            className="w-full h-64 object-contain rounded-lg mb-4"
          />
          <h2 className="text-2xl font-bold mb-2 text-center">{sanctuary.name}</h2>
          <p className="text-sm mb-4 text-justify m-1">{sanctuary.info}</p>

          <p className="text-base mb-4 text-center">
            País: <strong>{sanctuary.country}</strong> <br />
            Más información en su página web:{" "}
            <a
              href={sanctuary.website}
              target="_blank"
              rel="noopener noreferrer"
              className="text-green-500 hover:underline m-1 "
            >
              {sanctuary.website}
            </a>
          </p>

          <div className="flex space-x-4">
            {sanctuary.socialMedia?.facebook && (
              <a
                href={sanctuary.socialMedia.facebook}
                target="_blank"
                rel="noopener noreferrer"
                className="text-blue-500 hover:underline"
              >
                Facebook
              </a>
            )}
            {sanctuary.socialMedia?.instagram && (
              <a
                href={sanctuary.socialMedia.instagram}
                target="_blank"
                rel="noopener noreferrer"
                className="text-pink-500 hover:underline"
              >
                Instagram
              </a>
            )}
            {sanctuary.socialMedia?.twitter && (
              <a
                href={sanctuary.socialMedia.twitter}
                target="_blank"
                rel="noopener noreferrer"
                className="text-blue-400 hover:underline"
              >
                Twitter
              </a>
            )}
          </div>

          {sanctuary.donationLink && (
            <a
              href={sanctuary.donationLink}
              target="_blank"
              rel="noopener noreferrer"
              className="bg-green-500 text-white py-2 px-4 rounded hover:bg-green-600 transition duration-200 mt-4"
            >
              Donar al Santuario
            </a>
          )}
        </div>
      </div>
    </div>
  );
}
