import React, { useState } from "react";
import { X, ChevronLeft, ChevronRight } from "lucide-react";

export default function ModalCarousel({ isOpen, onClose, sanctuaries }) {
  const [currentIndex, setCurrentIndex] = useState(0);

  if (!isOpen || sanctuaries.length === 0) return null;

  const currentSanctuary = sanctuaries[currentIndex];

  const goNext = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === sanctuaries.length - 1 ? 0 : prevIndex + 1
    );
  };

  const goPrevious = () => {
    setCurrentIndex((prevIndex) =>
      prevIndex === 0 ? sanctuaries.length - 1 : prevIndex - 1
    );
  };

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center bg-black bg-opacity-70 font-mono w-full h-full overflow-y-hidden">
      <div className="bg-white text-black rounded-lg shadow-lg w-full max-w-lg sm:max-w-2xl sm:mx-4 mx-2 relative p-6">
        {/* Botón para cerrar el modal */}
        <button
          onClick={onClose}
          className="absolute top-4 right-4 text-gray-500 hover:text-gray-700 focus:outline-none"
        >
          <X className="w-6 h-6" />
        </button>

        {/* Carrusel de Santuarios */}
        <div className="relative flex items-center justify-center w-full">
          {/* Botón anterior */}
          <button
            onClick={goPrevious}
            className="absolute left-0 ml-2 text-gray-500 hover:text-gray-700 focus:outline-none z-10"
            style={{ top: '50%', transform: 'translateY(-50%)' }} // Centrar el botón verticalmente
          >
            <ChevronLeft className="w-10 h-10" />
          </button>

          {/* Contenido del santuario actual */}
          <div className="flex flex-col items-center text-center w-full mx-4"> {/* Ajuste para que el contenido del carrusel no se desborde */}
            <img
              src={currentSanctuary.imageUrl}
              alt={currentSanctuary.name}
              className="w-full h-64 object-contain rounded-lg mb-4"
            />
            <h2 className="text-2xl font-bold mb-2">{currentSanctuary.name}</h2>
            <p className="text-sm mb-4 text-justify">{currentSanctuary.info}</p>
            <p className="text-base mb-4">
              País: <strong>{currentSanctuary.country}</strong>
            </p>
            {currentSanctuary.website && (
              <p className="text-base mb-4">
                Sitio web:{" "}
                <a
                  href={currentSanctuary.website}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-green-500 hover:underline"
                >
                  {currentSanctuary.website}
                </a>
              </p>
            )}
            <div className="flex space-x-4">
              {currentSanctuary.socialMedia?.facebook && (
                <a
                  href={currentSanctuary.socialMedia.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-blue-500 hover:underline"
                >
                  Facebook
                </a>
              )}
              {currentSanctuary.socialMedia?.instagram && (
                <a
                  href={currentSanctuary.socialMedia.instagram}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-pink-500 hover:underline"
                >
                  Instagram
                </a>
              )}
              {currentSanctuary.socialMedia?.twitter && (
                <a
                  href={currentSanctuary.socialMedia.twitter}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="text-blue-400 hover:underline"
                >
                  Twitter
                </a>
              )}
            </div>
            {currentSanctuary.donationLink && (
              <a
                href={currentSanctuary.donationLink}
                target="_blank"
                rel="noopener noreferrer"
                className="bg-green-500 text-white py-2 px-4 rounded-lg hover:bg-green-600 transition duration-200"
              >
                Donar al Santuario
              </a>
            )}
          </div>

          {/* Botón siguiente */}
          <button
            onClick={goNext}
            className="absolute right-0 mr-2 text-gray-500 hover:text-gray-700 focus:outline-none z-10"
            style={{ top: '50%', transform: 'translateY(-50%)' }} // Centrar el botón verticalmente
          >
            <ChevronRight className="w-10 h-10" />
          </button>
        </div>
      </div>
    </div>
  );
}
