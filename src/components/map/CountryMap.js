/*import React, { useEffect, useState } from "react";
import dynamic from "next/dynamic";
import L from "leaflet";

// Cargar dinámicamente los componentes de Leaflet solo en el lado del cliente
const MapContainer = dynamic(() => import("react-leaflet").then(mod => mod.MapContainer), { ssr: false });
const TileLayer = dynamic(() => import("react-leaflet").then(mod => mod.TileLayer), { ssr: false });
const Marker = dynamic(() => import("react-leaflet").then(mod => mod.Marker), { ssr: false });
const Popup = dynamic(() => import("react-leaflet").then(mod => mod.Popup), { ssr: false });

// Icono personalizado para los marcadores
const animalIcon = new L.Icon({
  iconUrl: "/images/pig_icon.svg", // Asegúrate de que la URL sea correcta
  iconSize: [40, 40],
});

export default function CountryMap({ onSelectCountry, closeModal }) {
  const [isClient, setIsClient] = useState(false);

  // Comprobamos si estamos en el lado del cliente
  useEffect(() => {
    if (typeof window !== "undefined") {
      setIsClient(true);
    }
  }, []);

  if (!isClient) {
    return null; // No renderizamos en el servidor
  }

  const countries = [
    { id: 1, name: "España", coordinates: [40.4637, -3.7492] },
    { id: 2, name: "México", coordinates: [23.6345, -102.5528] },
    { id: 3, name: "Chile", coordinates: [-35.6751, -71.5430] },
    { id: 4, name: "Brasil", coordinates: [-14.2350, -51.9253] },
    { id: 5, name: "Japón", coordinates: [36.2048, 138.2529] }
  ];

  return (
    <MapContainer center={[20, 0]} zoom={2} style={{ height: "500px", width: "100%" }}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      />
      {countries.map((country) => (
        <Marker key={country.id} position={country.coordinates} icon={animalIcon}>
          <Popup>
            <button
              className="text-blue-500 underline"
              onClick={() => {
                onSelectCountry(country.id);
                closeModal();
              }}
            >
              {country.name}
            </button>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
}
*/