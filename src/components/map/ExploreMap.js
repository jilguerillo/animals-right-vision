import React, { useState } from "react";
import CountryMap from "@/components/map/CountryMap";
import Modal from "@/components/ui/Modal";

export default function ExploreMap() {
  const [isMapOpen, setMapOpen] = useState(true);

  const closeMapModal = () => {
    setMapOpen(false);
  };

  //<CountryMap
  //      onSelectCountry={handleSelectCountry}
  //      closeModal={closeMapModal}  // Pasar la función closeModal aquí
  ///>

  const handleSelectCountry = (countryId) => {
    console.log("Selected country ID:", countryId);
    closeMapModal(); // Cerrar el modal al seleccionar un país
  };

  return (
    <Modal isOpen={isMapOpen} onClose={closeMapModal}>
      <h2 className="text-2xl font-bold mb-4">Explora Santuarios por País</h2>
      
      
      
    </Modal>
  );
}
