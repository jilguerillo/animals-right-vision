import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

export default function Carousel({ categories, onSelectCategory }) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000, // Tiempo entre cambios automáticos de imagen
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  return (
    <div className="px-8 py-4">
      <Slider {...settings}>
        {categories.map((category) => (
          <div key={category.id} className="p-2">
            <div
              className="relative h-48 rounded-lg overflow-hidden shadow-sm cursor-pointer transition-transform transform hover:scale-105"
              onClick={() => onSelectCategory(category.id)}
              style={{ backgroundColor: '#eeeee4' }} // Color de fondo como el de las imágenes
            >
              <img
                src={category.imageUrl}
                alt={category.name}
                className="w-full h-full object-contain" // object-contain para no cortar la imagen
              />
              <div className="absolute bottom-0 left-0 p-4 bg-gradient-to-t from-slate-600 via-black/60 to-transparent text-white w-full">
                <h3 className="text-lg font-bold">{category.name}</h3>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
}
