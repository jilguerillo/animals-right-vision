module.exports = {
    reactStrictMode: true,  // Habilita el modo estricto de React para ayudar con debugging
    swcMinify: true,        // Habilita el minificador SWC para optimizar el código
    images: {
      domains: ['www.youtube.com', 'vimeo.com', 'via.placeholder.com'],  // Agrega dominios permitidos para imágenes o videos embebidos
    },
    webpack: (config) => {
      // Configuración de alias de rutas
      config.resolve.alias['@'] = require('path').resolve(__dirname, 'src');
      return config;
    },
  };