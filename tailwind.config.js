/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}', // Esto incluye los archivos de tu proyecto
  ],
  theme: {
    extend: {
      colors: {
        greenDark: '#384D48',
        grayDark: '#6E7271',
        greenLight: '#ACAD94',
        grayLight: '#D8D4D5',
        grayLighter: '#E2E2E2',
      },
    },
  },
  plugins: [],
};
