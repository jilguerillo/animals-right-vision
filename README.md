# Animal Right Vision: Unifying Animal and Environmental Aid Globally



## Join the fight for a more compassionate and sustainable world!

Animal Right Vision is a web platform that aims to centralize and facilitate access to information about organizations and groups around the world that work for animal welfare and environmental protection.

[Website ALPHA stage](https://animals-right-vision.vercel.app/)

## What will you find in Animal Right Vision?

- A comprehensive database: Search and discover organizations and groups by continent, country, region or locality.
- Detailed information: Access summaries of each organization's history, trajectory, social networks and donation links.
- Advanced filters: Easily find organizations that align with your specific interests and causes.
- Connect and collaborate: Join the Animal Right Vision community to share information, experiences and opportunities for action.

```
cd existing_repo
git remote add origin https://gitlab.com/jilguerillo/animals-right-vision.git
git branch -M main
git push -uf origin main
```

## Who is this platform for?

Animal Right Vision is aimed at all audiences who care about animal welfare and the protection of the planet. It is especially useful for:

- People interested in supporting organizations and groups: Animal Right Vision allows you to easily find organizations that align with your values and causes.
- Members of the animal liberation movement: The platform gives you a global overview of the movement and connects you with other people who share your passion.
- Students and researchers: Animal Right Vision can be a valuable tool for learning about different approaches to animal and environmental advocacy.

## How can you join the cause?

- Visit our website: https://gitlab.com/gitlab-org/gitlab
- Explore the database of organizations: https://gitlab.com/gitlab-org/gitlab
- Share Animal Right Vision with your friends and family.
- Donate to the organizations that inspire you.
- Volunteer your time and skills.
- Together we can create a positive change in the world!
